# gitlab-ci templates

## Python
### Linter

To Add the pylint badge:
  Your project -> Settings -> General -> Badges. Add fill the form (<project_path> i.e. https://gitlab.com/utils9/gitlab-ci-templates)

|||
|---|---|
|Name|pylint|
|Link|<project_url>/commits/master|
|Badge Image URL | <project_url>/-/jobs/artifacts/master/raw/pylint.svg?job=lint:python:pylint |
